package doc

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

const (
	userC = "user"
)

type User struct {
	ID     	   bson.ObjectId `json:"-" bson:"_id"`
	Name   	   string        `json:"name"`
	Email      string        `json:"email"`
	Password   string        `json:"password"`
	Permission string        `json:"permission"`
	CreatedBy  string        `json:"-"`
	UpdatedBy  string        `json:"-"`
	CreatedAt  time.Time	 `json:"-"`
	UpdatedAt  time.Time	 `json:"-"`
}

func (u *User) GetC() string {
	return "user"
}

func (u *User) GetDoc() interface{} {
	u.ID = bson.NewObjectId()
	return u
}

func (u *User) GetID() bson.ObjectId {
	return u.ID
}