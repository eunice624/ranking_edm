package doc

import "github.com/globalsign/mgo/bson"

const (
	sample2C = "sample2"
)

type Sample2 struct {
	ID      bson.ObjectId `json:"-" bson:"_id"`
	Title   string        `json:"title"`
	Content string        `json:"content"`
}

func (s *Sample2) GetC() string {
	return "sample2"
}

func (s *Sample2) GetDoc() interface{} {
	s.ID = bson.NewObjectId()
	return s
}

func (s *Sample2) GetID() bson.ObjectId {
	return s.ID
}
