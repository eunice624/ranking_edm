package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	tagC = "tag"
)

type Tag struct {
	ID         bson.ObjectId `json:"-" bson:"_id"`
	Name   	   string		 `json:"name"`
	SourceID   bson.ObjectId `json:"sourceId"`
	SubTag	   []string		 `json:"subTag"`
	CreatedAt  time.Time	 `json:"createdAt"`
}

func (t *Tag) GetC() string {
	return "tag"
}

func (t *Tag) GetDoc() interface{} {
	t.ID = bson.NewObjectId()
	return t
}

func (t *Tag) GetID() bson.ObjectId {
	return t.ID
}

func (t *Tag) Save(db *mgo.Database) error {
	t.ID = bson.NewObjectId()
	return db.C(tagC).Insert(t)
}

func (t *Tag) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getTagObjectID(id)
	if err != nil {
		return err
	}
	return db.C(tagC).FindId(id).One(t)
}

func getTagObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type TagList []Tag

func (t TagList) GetTag() string {
	return tagC
}

func (t TagList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&t).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(t)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (t *TagList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(tagC).Find(q).All(t)
	if err != nil {
		return err
	}
	return nil
}
