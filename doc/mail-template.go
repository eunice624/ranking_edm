package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	mailTempC = "mailTemplate"
)

type MailTemplate struct {
	ID      	bson.ObjectId `json:"-" bson:"_id"`
	EventID   	bson.ObjectId `json:"eventId"`
	EventCodeId bson.ObjectId `json:"-"`
	TitleA   	string        `json:"titleA"`
	TitleB   	string        `json:"titleB"`
	SourceID    bson.ObjectId `json:"sourceId"`
	CustomerTag []string      `json:"customerTag"`
	SendTime    time.Time     `json:"sendTime"`
	IsABTest    bool      	  `json:"isABTest"`
	Status 		string        `json:"-"`
	SendAccount string        `json:"-"`
	SenderName 	string        `json:"senderName"`
	ReplyTo 	string        `json:"replyTo"`
	ReviewID    bson.ObjectId `json:"reviewId" bson:",omitempty"`
	IsRead 		bool          `json:"-"`
	CreatedBy 	string        `json:"-"`
	UpdatedBy 	string        `json:"-"`
	CreatedAt 	time.Time	  `json:"-"`
	UpdatedAt 	time.Time	  `json:"-"`
}

type SubmitArray []string

func (m *MailTemplate) GetC() string {
	return "mailTemplate"
}

func (m *MailTemplate) GetDoc() interface{} {
	m.ID = bson.NewObjectId()
	return m
}

func (m *MailTemplate) GetID() bson.ObjectId {
	return m.ID
}

func (m *MailTemplate) Save(db *mgo.Database) error {
	m.ID = bson.NewObjectId()
	return db.C(mailTempC).Insert(m)
}

func (m *MailTemplate) Update(db *mgo.Database, id interface{}) error {
	id, err := getMailTempObjectID(id)
	if err != nil {
		return err
	}
	selector := bson.M{"_id": id}
	return db.C(mailTempC).Update(selector, m)
}

func UpdateAll(db *mgo.Database, ids []interface{}, q bson.M) (interface{}, error) {
	var idArray []interface{}
	for _, id := range ids {
		id, err := getMailTempObjectID(id)
		if err != nil {
			return nil, err
		}
		idArray = append(idArray, id)
	}

	selector := bson.M{"_id": bson.M{"$in": idArray}}
	info, err := db.C(mailTempC).UpdateAll(selector, bson.M{"$set": q})
	return info, err
}

func (m *MailTemplate) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getMailTempObjectID(id)
	if err != nil {
		return err
	}
	return db.C(mailTempC).FindId(id).One(m)
}

func getMailTempObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type MailTempList []MailTemplate

func (m MailTempList) GetTag() string {
	return mailTempC
}

func (m MailTempList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&m).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(m)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (m *MailTempList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(mailTempC).Find(q).All(m)
	if err != nil {
		return err
	}
	return nil
}
