package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	eventC = "event"
)

type Event struct {
	ID        	 bson.ObjectId `json:"-" bson:"_id"`
	Key   	  	 string        `json:"key"`
	EventCodeId  bson.ObjectId `json:"-"`
	Name	  	 string        `json:"name"`
	StartDate 	 time.Time     `json:"startDate"`
	EndDate 	 time.Time     `json:"endDate"`
	Content 	 string        `json:"content"`
	CreatedBy 	 string        `json:"-"`
	UpdatedBy 	 string        `json:"-"`
	CreatedAt 	 time.Time	   `json:"-"`
	UpdatedAt 	 time.Time	   `json:"-"`
}

func (e *Event) GetC() string {
	return "event"
}

func (e *Event) GetDoc() interface{} {
	e.ID = bson.NewObjectId()
	return e
}

func (e *Event) GetID() bson.ObjectId {
	return e.ID
}

func (e *Event) Save(db *mgo.Database) error {
	e.ID = bson.NewObjectId()
	return db.C(eventC).Insert(e)
}

func (e *Event) Update(db *mgo.Database, id interface{}) error {
	id, err := getEventObjectID(id)
	if err != nil {
		return err
	}
	selector := bson.M{"_id": id}
	return db.C(eventC).Update(selector, e)
}

func (e *Event) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getEventObjectID(id)
	if err != nil {
		return err
	}
	return db.C(eventC).FindId(id).One(e)
}

func getEventObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type EventList []Event

func (e EventList) GetTag() string {
	return eventC
}

func (e EventList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&e).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(e)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (e *EventList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(eventC).Find(q).All(e)
	if err != nil {
		return err
	}
	return nil
}
