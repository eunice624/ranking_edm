package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	replyMailC = "replyMail"
)

type ReplyMail struct {
	ID        bson.ObjectId `json:"-" bson:"_id"`
	Email     string        `json:"email"`
	Comment   string        `json:"comment"`
	CreatedBy string        `json:"-"`
	UpdatedBy string        `json:"-"`
	CreatedAt time.Time	    `json:"-"`
	UpdatedAt time.Time	    `json:"-"`
}

func (r *ReplyMail) GetC() string {
	return "replyMail"
}

func (r *ReplyMail) GetDoc() interface{} {
	r.ID = bson.NewObjectId()
	return r
}

func (r *ReplyMail) GetID() bson.ObjectId {
	return r.ID
}

func (r *ReplyMail) Save(db *mgo.Database) error {
	r.ID = bson.NewObjectId()
	return db.C(replyMailC).Insert(r)
}

func (r *ReplyMail) Update(db *mgo.Database, id interface{}) error {
	id, err := getReplyObjectID(id)
	if err != nil {
		return err
	}
	selector := bson.M{"_id": id}
	return db.C(replyMailC).Update(selector, r)
}

func (r *ReplyMail) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getReplyObjectID(id)
	if err != nil {
		return err
	}
	return db.C(replyMailC).FindId(id).One(r)
}

func getReplyObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type ReplyMailList []ReplyMail

func (r ReplyMailList) GetTag() string {
	return replyMailC
}

func (r ReplyMailList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&r).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(r)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (r *ReplyMailList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(replyMailC).Find(q).All(r)
	if err != nil {
		return err
	}
	return nil
}
