package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	sampleC = "sample"
)

type Sample struct {
	ID      bson.ObjectId `json:"-" bson:"_id"`
	Title   string        `json:"title"`
	Tags    []string      `json:"tags"`
	Content string        `json:"content"`
}

func (s *Sample) Save(db *mgo.Database) error {
	s.ID = bson.NewObjectId()
	return db.C(sampleC).Insert(s)
}

func (s *Sample) FindByID(db *mgo.Database, id interface{}) error {
	id, err := GetObjectID(id)
	if err != nil {
		return err
	}
	return db.C(sampleC).FindId(id).One(s)
}

func GetObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type SampleList []Sample

func (s SampleList) GetTag() string {
	return sampleC
}

func (s SampleList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&s).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(s)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (s *SampleList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(sampleC).Find(q).All(s)
	if err != nil {
		return err
	}
	return nil
}

func Format(inter interface{}, f func(i interface{}) map[string]interface{}) interface{} {
	ik := reflect.TypeOf(inter).Kind()
	if ik == reflect.Ptr {
		return f(inter)
	}
	if ik != reflect.Slice {
		return nil
	}
	v := reflect.ValueOf(inter)
	l := v.Len()
	ret := make([]map[string]interface{}, l)
	for i := 0; i < l; i++ {
		ret[i] = f(v.Index(i).Interface())
	}
	return ret
}
