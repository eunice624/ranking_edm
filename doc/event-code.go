package doc

import (
	"bytes"
	"net/http"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	eventCodeC = "eventCode"
)

type EventCode struct {
	ID        	 bson.ObjectId 	  	`json:"-" bson:"_id"`
	Code   	  	 string        	  	`json:"code"`
	Comment	  	 string        	  	`json:"comment"`
	ReviewConfID bson.ObjectId    	`json:"reviewConfId"`
	CreatedBy 	 string        	  	`json:"-"`
	UpdatedBy 	 string        	  	`json:"-"`
	CreatedAt 	 time.Time	   	  	`json:"-"`
	UpdatedAt 	 time.Time	   	  	`json:"-"`
	ReviewConfig []LookUpReviewConf `json:"-" bson:"reviewConfig,omitempty"`
}

type LookUpReviewConf struct {
	ID      bson.ObjectId `json:"id,omitempty" bson:"_id,omitempty"`
	Title   string        `json:"title,omitempty" bson:"title,omitempty"`
	Content []Content 	  `json:"content,omitempty" bson:"content,omitempty"`
}

func (e *EventCode) GetC() string {
	return "eventCode"
}

func (e *EventCode) GetDoc() interface{} {
	e.ID = bson.NewObjectId()
	return e
}

func (e *EventCode) GetID() bson.ObjectId {
	return e.ID
}

func (e *EventCode) Save(req *http.Request, db *mgo.Database) error {
	e.ID = bson.NewObjectId()

	currentTime := time.Now()
	e.CreatedAt = currentTime
	e.UpdatedAt = currentTime

	e.CreatedBy = req.Header.Get("AuthID")
	e.UpdatedBy = req.Header.Get("AuthID")

	return db.C(eventCodeC).Insert(e)
}

func (e *EventCode) Update(req *http.Request, db *mgo.Database, id interface{}) error {
	id, err := getEventCodeObjectID(id)
	if err != nil {
		return err
	}
	selector := bson.M{"_id": id}

	currentTime := time.Now()
	e.UpdatedAt = currentTime

	e.UpdatedBy = req.Header.Get("AuthID")

	return db.C(eventCodeC).Update(selector, e)
}

func (e *EventCode) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getEventCodeObjectID(id)
	if err != nil {
		return err
	}
	return db.C(eventCodeC).FindId(id).One(e)
}

func getEventCodeObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type EventCodeList []EventCode

func (e EventCodeList) GetTag() string {
	return eventCodeC
}

func (e EventCodeList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&e).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(e)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (e *EventCodeList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(eventCodeC).Find(q).All(e)
	if err != nil {
		return err
	}
	return nil
}
