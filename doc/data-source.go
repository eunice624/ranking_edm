package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	sourceC = "source"
)

type Source struct {
	ID        	bson.ObjectId `json:"-" bson:"_id"`
	Source    	string        `json:"source"`
	TagURL    	string        `json:"tagURL"`
	CustomerURL string     	  `json:"customerURL"`
	SenderURL 	string        `json:"senderURL"`
	CreatedBy 	string        `json:"-"`
	UpdatedBy 	string     	  `json:"-"`
	CreatedAt 	time.Time	  `json:"-"`
	UpdatedAt 	time.Time	  `json:"-"`
}

func (s *Source) Save(db *mgo.Database) error {
	s.ID = bson.NewObjectId()
	return db.C(sourceC).Insert(s)
}

func (s *Source) Update(db *mgo.Database, id interface{}) error {
	id, err := getSourceObjectID(id)
	if err != nil {
		return err
	}
	selector := bson.M{"_id": id}
	return db.C(sourceC).Update(selector, s)
}

func (s *Source) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getSourceObjectID(id)
	if err != nil {
		return err
	}
	return db.C(sourceC).FindId(id).One(s)
}

func getSourceObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type SourceList []Source

func (s SourceList) GetTag() string {
	return sourceC
}

func (s SourceList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&s).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(s)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (s *SourceList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(sourceC).Find(q).All(s)
	if err != nil {
		return err
	}
	return nil
}
