package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	reviewConfigC = "reviewConfig"
)

type ReviewConfig struct {
	ID        bson.ObjectId `json:"-" bson:"_id"`
	Title     string        `json:"title"`
	Content   []Content 	`json:"content"`
	CreatedBy string        `json:"-"`
	CreatedAt time.Time	 	`json:"-"`
	UpdatedBy string     	`json:"-"`
	UpdatedAt time.Time	 	`json:"-"`
}

type Content struct {
	Name  string `json:"name"`
	Email string `json:"email"`
}

func (r *ReviewConfig) GetC() string {
	return "reviewConfig"
}

func (r *ReviewConfig) GetDoc() interface{} {
	r.ID = bson.NewObjectId()
	return r
}

func (r *ReviewConfig) GetID() bson.ObjectId {
	return r.ID
}

func (r *ReviewConfig) Save(db *mgo.Database) error {
	r.ID = bson.NewObjectId()
	return db.C(reviewConfigC).Insert(r)
}

func (r *ReviewConfig) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getReviewConObjectID(id)
	if err != nil {
		return err
	}
	return db.C(reviewConfigC).FindId(id).One(r)
}

func getReviewConObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type ReviewConfigList []ReviewConfig

func (r ReviewConfigList) GetTag() string {
	return reviewConfigC
}

func (r ReviewConfigList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&r).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(r)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (r *ReviewConfigList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(reviewConfigC).Find(q).All(r)
	if err != nil {
		return err
	}
	return nil
}
