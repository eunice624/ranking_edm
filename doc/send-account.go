package doc

import (
	"bytes"
	"encoding/gob"
	"errors"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

const (
	sendAccountC = "sendAccount"
)

type SendAccount struct {
	ID         bson.ObjectId `json:"-" bson:"_id"`
	Name   	   string        `json:"name"`
	Account    string      	 `json:"account"`
	Password   string        `json:"password"`
	Domain     string      	 `json:"domain"`
	CreatedBy  string        `json:"-"`
	UpdatedBy  string        `json:"-"`
	CreatedAt  time.Time	 `json:"-"`
	UpdatedAt  time.Time	 `json:"-"`
}

func (s *SendAccount) GetC() string {
	return "sendAccount"
}

func (s *SendAccount) GetDoc() interface{} {
	s.ID = bson.NewObjectId()
	return s
}

func (s *SendAccount) GetID() bson.ObjectId {
	return s.ID
}

func (s *SendAccount) Save(db *mgo.Database) error {
	s.ID = bson.NewObjectId()
	return db.C(sendAccountC).Insert(s)
}

func (s *SendAccount) Update(db *mgo.Database, id interface{}) error {
	id, err := getSAccountObjectID(id)
	if err != nil {
		return err
	}
	selector := bson.M{"_id": id}
	return db.C(sendAccountC).Update(selector, s)
}

func (s *SendAccount) FindByID(db *mgo.Database, id interface{}) error {
	id, err := getSAccountObjectID(id)
	if err != nil {
		return err
	}
	return db.C(sendAccountC).FindId(id).One(s)
}

func getSAccountObjectID(id interface{}) (*bson.ObjectId, error) {
	var myID bson.ObjectId
	switch dtype := reflect.TypeOf(id).String(); dtype {
	case "string":
		str := id.(string)
		if str == "" || !bson.IsObjectIdHex(str) {
			return nil, errors.New("id is error: " + str)
		}
		myID = bson.ObjectIdHex(str)
	case "bson.ObjectId":
		myID = id.(bson.ObjectId)
	default:
		return nil, errors.New("not support type: " + dtype)
	}
	return &myID, nil
}

type SendAccountList []SendAccount

func (s SendAccountList) GetTag() string {
	return sendAccountC
}

func (s SendAccountList) GetByte(db *mgo.Database, q bson.M) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := (&s).Find(db, q)
	if err != nil {
		return nil, err
	}
	err = enc.Encode(s)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (s *SendAccountList) Find(db *mgo.Database, q bson.M) error {
	err := db.C(sendAccountC).Find(q).All(s)
	if err != nil {
		return err
	}
	return nil
}
