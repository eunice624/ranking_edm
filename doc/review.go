package doc

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

const (
	reviewC = "review"
)

type Review struct {
	ID        	  bson.ObjectId 	  `json:"-" bson:"_id"`
	Key     	  string        	  `json:"title"`
	EventID   	  bson.ObjectId  	  `json:"eventId"`
	MailID   	  []bson.ObjectId  	  `json:"mailId"`
	ReviewConfig  []ReviewConfContent `json:"reviewConfig"`
	CurrentReview int				  `json:"currentReview"`
	Status   	  string 			  `json:"status"`
	CreatedBy 	  string        	  `json:"-"`
	CreatedAt 	  time.Time	 		  `json:"-"`
	UpdatedBy 	  string     		  `json:"-"`
	UpdatedAt 	  time.Time	 		  `json:"-"`
	MailTemplate  []MailTemplate      `json:"-" bson:"mailTemplate,omitempty"`
}

type ReviewConfContent struct {
	Name   	   string    `json:"name"`
	Email  	   string    `json:"email"`
	Status 	   string    `json:"status"`
	Comment	   string    `json:"comment,omitempty" bson:",omitempty"`
	ReviewTime time.Time `json:"reviewTime,omitempty" bson:",omitempty"`
}

func (r *Review) GetC() string {
	return "review"
}

func (r *Review) GetDoc() interface{} {
	r.ID = bson.NewObjectId()
	return r
}

func (r *Review) GetID() bson.ObjectId {
	return r.ID
}
