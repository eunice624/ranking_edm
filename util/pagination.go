package util

import (
	"errors"
	"math"
	"time"

	"github.com/globalsign/mgo"
)

type PaginationRes struct {
	Rows     interface{} `json:"rows,omitempty"`
	Total    int         `json:"total,omitempty"`
	AllPages int         `json:"allPages,omitempty"`
	Page     int         `json:"page,omitempty"`
	Limit    int         `json:"limit,omitempty"`
}

const (
	MaxLimit = 300
)

func MongoPagination(query *mgo.Query, limit int, page int) (*PaginationRes, error) {
	total, err := query.Count()

	if err != nil {
		return nil, err
	}
	if total == 0 {
		return nil, errors.New("query data is 0")
	}
	if limit < 1 || MaxLimit > MaxLimit {
		limit = 100
	}
	totalPage := int(math.Ceil(float64(total) / float64(limit)))

	if page > totalPage {
		page = totalPage
	} else if page < 1 {
		page = 1
	}
	return &PaginationRes{Total: total, AllPages: totalPage, Page: page, Limit: limit}, nil
}

func DateTimePagination(startTime, endTime time.Time, intervalSec int64, limit, page int, isASC bool) (*PaginationRes, time.Time, time.Time) {
	startSec := startTime.Unix()
	endSec := endTime.Unix()
	total := math.Ceil(float64(endSec-startSec+1) / float64(intervalSec))
	if limit < 1 || MaxLimit > MaxLimit {
		limit = 100
	}
	allPage := int(math.Ceil(total / float64(limit)))
	if page > allPage {
		page = allPage
	} else if page < 1 {
		page = 1
	}
	if allPage == 1 {
		return &PaginationRes{Total: int(total), AllPages: allPage, Page: page, Limit: limit}, startTime, endTime
	}
	if isASC {
		startSec += (intervalSec * int64(limit)) * (int64(page - 1))
		queryEndSec := startSec + (intervalSec * int64(limit)) - 1
		if endSec > queryEndSec {
			endSec = queryEndSec
		}
	} else {
		endSec -= (intervalSec * int64(limit)) * (int64(page) - 1)
		queryStartSec := endSec - (intervalSec * int64(limit)) + 1
		if startSec < queryStartSec {
			startSec = queryStartSec
		}
	}
	return &PaginationRes{Total: int(total), AllPages: allPage, Page: page, Limit: limit}, time.Unix(startSec, 0), time.Unix(endSec, 0)
}
