# EDM

merge test

## 系統需求

[mongoDB](https://docs.mongodb.com/v4.0/)

[Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/index.html)

[redis](https://redis.io)

## Docker開發環境

執行服務

```sh
docker-compose up -d
```

關閉服務
```sh
docker-compose stop
```