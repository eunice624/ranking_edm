package model

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"net/http"
	"ranking-edm/rsrc"
	"reflect"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/go-redis/redis"
)

type DaoInter interface {
	GetTag() string
	GetByte(db *mgo.Database, q bson.M) ([]byte, error)
}

type cacheModel struct {
	redis    *redis.Client
	duration time.Duration
}

func GetCatcheModelByReq(req *http.Request, d string) *cacheModel {
	db := rsrc.GetDI().GetRedisByReq(req)
	return GetCacheModel(db, d)
}

func GetCacheModel(r *redis.Client, d string) *cacheModel {
	duration, err := time.ParseDuration(d)
	if err != nil {
		panic(err.Error())
	}
	return &cacheModel{
		redis:    r,
		duration: duration,
	}
}

func (cm *cacheModel) MgoFind(dao DaoInter, q bson.M, mgo *mgo.Database) error {
	key := fmt.Sprintf("%s:%v", dao.GetTag(), q)
	d, err := cm.redis.Get(key).Bytes()
	if err != nil {
		fmt.Println("new")
		d, err = dao.GetByte(mgo, q)
		if err != nil {
			return err
		}
		cm.redis.Set(key, d, cm.duration)
	}
	b := bytes.NewBuffer(d)
	err = gob.NewDecoder(b).Decode(dao)
	if err != nil {
		return err
	}
	return nil
}

func (cm *cacheModel) MgoFindV2(doc docInter, q bson.M, dbmodel *mgoDBModel) (interface{}, error) {
	key := fmt.Sprintf("%s:%v", doc.GetC(), q)
	d, err := cm.redis.Get(key).Bytes()
	if err != nil {
		r, err := dbmodel.Find(doc, q, 0, 0)
		if err != nil {
			return nil, err
		}
		d, err = toBytes(r)
		if err != nil {
			return nil, err
		}
		cm.redis.Set(key, d, cm.duration)
		return r, nil
	}
	myType := reflect.TypeOf(doc)
	sliceOfT := reflect.SliceOf(myType)
	ptr := reflect.New(sliceOfT).Interface()
	b := bytes.NewBuffer(d)
	err = gob.NewDecoder(b).Decode(ptr)
	if err != nil {
		return nil, err
	}
	return reflect.ValueOf(ptr).Elem().Interface(), nil
}

func toBytes(o interface{}) ([]byte, error) {
	var b bytes.Buffer
	enc := gob.NewEncoder(&b)
	err := enc.Encode(o)
	if err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}
