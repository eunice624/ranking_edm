package model

import (
	"context"

	elastic "gopkg.in/olivere/elastic.v7"
)

type searchClient interface {
	GetIndexByDate(t string) string
	GetClient(k string) *elastic.Client
	ExistsTypeByDate(k string,t string) bool
	CreateTypeByDate(k string,t string, mapping string) error
}

type searchDao interface{
	GetType() string
	GetJsonBody() (string,error)
	GetID() string
	GetMapping() string
}

type searchModel struct {
	client searchClient
	cKey string
}

func GetSearchModel(c searchClient, key string) *searchModel {
	return &searchModel{
		client: c,
		cKey: key,
	}
}

func (sm *searchModel) Put(sd searchDao) error {
	if !sm.client.ExistsTypeByDate(sm.cKey, sd.GetType()) {
		sm.client.CreateTypeByDate(sm.cKey, sd.GetType(), sd.GetMapping())
	}
	body,err := sd.GetJsonBody()
	if err != nil {
		return err
	}
	index := sm.client.GetIndexByDate(sd.GetType())
	client := sm.client.GetClient(sm.cKey)

	_, err = client.Index().
		Index(index).
		Id(sd.GetID()).
		BodyJson(body).Do(context.Background())

	if err != nil {
		return err
	}

	_, err = client.Flush().Index(index).Do(context.Background())
	if err != nil {
		return  err
	}
	return nil
}



