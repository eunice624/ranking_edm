package model

import (
	"fmt"
	"testing"
	"time"

	"ranking-edm/rsrc/db"
	"ranking-edm/search"

	"github.com/stretchr/testify/assert"
)

func getSearchClient() searchClient {
	return &db.Search{
		Hosts: []string{"http://localhost:9200"},
		Index: "test",
	}
}

func Test_Search_Put(t *testing.T) {
	s := &search.Sample{
		ID:      "4",
		Name:    "peter4",
		Content: "this is content",
		Time:    time.Now(),
		Age:     20,
	}
	sm := GetSearchModel(getSearchClient(), "test1234")
	err := sm.Put(s)
	fmt.Println(err)
	assert.False(t, true)
}
