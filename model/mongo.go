package model

import (
	"errors"
	"net/http"
	"reflect"

	"ranking-edm/doc"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/globalsign/mgo/txn"
)

type mgoDBModel struct {
	db *mgo.Database
}

func GetMgoDBModelByReq(req *http.Request) *mgoDBModel {
	db := rsrc.GetDI().GetMgodbByReq(req)
	return &mgoDBModel{db: db}
}

func GetMgoDBModel(c *mgo.Database) *mgoDBModel {
	return &mgoDBModel{db: c}
}

type docInter interface {
	GetC() string
	GetDoc() interface{}
	GetID() bson.ObjectId
}

type pipelineQryInter interface {
	GetQ() bson.M
	GetPipleLine() []bson.M
}

const (
	TxnC = "txn"
)

func (mm *mgoDBModel) RunTxn(ops []txn.Op) error {
	r := txn.NewRunner(mm.db.C(TxnC))
	return r.Run(ops, bson.NewObjectId(), nil)
}

func (mm *mgoDBModel) Save(d docInter) error {
	return mm.db.C(d.GetC()).Insert(d.GetDoc())
}

func (mm *mgoDBModel) UpdateByID(d docInter, q bson.M) error {
	return mm.db.C(d.GetC()).UpdateId(d.GetID(), bson.M{"$set": q})
}

func (mm *mgoDBModel) UpdateAll(d docInter, selector bson.M, q bson.M) (int, error) {
	info, err := mm.db.C(d.GetC()).UpdateAll(selector, bson.M{"$set": q})
	return info.Updated, err
}

func (mm *mgoDBModel) RemoveByID(d docInter) error {
	return mm.db.C(d.GetC()).Remove(bson.M{"_id": d.GetID()})
}

func (mm *mgoDBModel) RemoveAll(d docInter, q bson.M) (int, error) {
	info, err := mm.db.C(d.GetC()).RemoveAll(q)
	return info.Removed, err
}

func (mm *mgoDBModel) FindByID(d docInter) error {
	if !d.GetID().Valid() {
		return errors.New("invalid id")
	}
	return mm.db.C(d.GetC()).FindId(d.GetID()).One(d)
}

func (mm *mgoDBModel) FindOne(d docInter, q bson.M) error {
	return mm.db.C(d.GetC()).Find(q).One(d)
}

func (mm *mgoDBModel) Find(d docInter, q bson.M, limit, page int) (interface{}, error) {
	myType := reflect.TypeOf(d)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()
	mgoQ := mm.db.C(d.GetC()).Find(q)
	if limit > 0 {
		mgoQ = mgoQ.Limit(limit)
	}
	if page > 0 {
		mgoQ = mgoQ.Skip(limit * (page - 1))
	}
	err := mgoQ.All(&slice)
	return slice, err
}

func (mm *mgoDBModel) Pipeline(d docInter, p []bson.M) (interface{}, error) {
	myType := reflect.TypeOf(d)
	slice := reflect.MakeSlice(reflect.SliceOf(myType), 0, 0).Interface()
	pipe := mm.db.C(d.GetC()).Pipe(p)
	err := pipe.All(&slice)
	return slice, err
}

func (mm *mgoDBModel) Iter(d docInter, q bson.M) *mgo.Iter {
	return mm.db.C(d.GetC()).Find(q).Iter()
}

const (
	MaxLimit = 200
)

func (mm *mgoDBModel) Paginator(d docInter, q bson.M, limit, page int,
	format func(i interface{}) map[string]interface{}) (*util.PaginationRes, error) {
	mgoQuery := mm.db.C(d.GetC()).Find(q)
	paginator, err := util.MongoPagination(mgoQuery, limit, page)
	if err != nil {
		return nil, err
	}
	result, err := mm.Find(d, q, paginator.Limit, paginator.Page)
	if err != nil {
		return nil, err
	}
	if format != nil {
		paginator.Rows = doc.Format(result, format)
	} else {
		paginator.Rows = result
	}
	return paginator, nil
}

func (mm *mgoDBModel) PaginatorPipeline(d docInter, pq pipelineQryInter, limit, page int,
	format func(i interface{}) map[string]interface{}) (*util.PaginationRes, error) {
	mgoQuery := mm.db.C(d.GetC()).Find(pq.GetQ())
	paginator, err := util.MongoPagination(mgoQuery, limit, page)
	if err != nil {
		return nil, err
	}
	pl := pq.GetPipleLine()
	pl = append(pl,
		bson.M{
			"$skip": paginator.Limit * (paginator.Page - 1),
		},
		bson.M{
			"$limit": paginator.Limit,
		})
	result, err := mm.Pipeline(d, pl)
	if err != nil {
		return nil, err
	}
	if format != nil {
		paginator.Rows = doc.Format(result, format)
	} else {
		paginator.Rows = result
	}
	return paginator, nil
}
