package api

import (
	"net/http"

	"github.com/gorilla/mux"
)

const (
	StatusRemote = 106
)

type APIConf struct {
	Port        string `yaml:"port,omitempty"`
	AllowSystem string `yaml:"allowSys,omitempty"`
	Middle      struct {
		GenDoc bool `yaml:"gen_doc,omitempty"`
		Auth   bool `yaml:"auth,omitempty"`
		Log    bool `yaml:"log,omitempty"`
		Access bool `yaml:"access,omitempty"`
		Debug  bool `yaml:"debug,omitempty"`
	} `yaml:"middle,omitempty"`
}

type MyAPI struct {
	Router      *mux.Router
	MiddleWares *[]Middleware
}

type APIHandler struct {
	Path       string
	Next       func(http.ResponseWriter, *http.Request)
	Method     string
	Auth       bool
	AllowMulti bool // 允許Token驗證就存取，不用判定裝置重覆登入使用
	System     []string
	Group      []string
}

type API interface {
	GetAPIs() *[]*APIHandler
	Enable() bool
}

func InitAPI(conf *MyAPI, apis ...API) {
	for _, myapi := range apis {
		if myapi.Enable() {
			addHandler(conf, myapi.GetAPIs())
		}
	}
}

func addHandler(conf *MyAPI, apiHandlers *[]*APIHandler) {
	router := conf.Router
	for _, handler := range *apiHandlers {
		//AddAuthPath(fmt.Sprintf("%s:%s", handler.Path, handler.Method), handler.Auth, handler.AllowMulti)
		router.HandleFunc(handler.Path, BuildChain(handler.Next, *conf.MiddleWares...)).Methods(handler.Method)
	}
}
