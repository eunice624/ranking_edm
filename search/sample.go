package search

import (
	"encoding/json"
	"time"

	"ranking-edm/rsrc"
	"ranking-edm/rsrc/db"
) 

type Sample struct {
	ID      string	`json:"id"`
	Name    string	`json:"name"`
	Content string	`json:"content"`
	Time time.Time	`json:"time"`
	Age int			`json:"age"`
}

func (s *Sample) GetType() string {
	return "sample"
}

func (s *Sample) GetID() string {
	return s.ID
}

func (s *Sample) GetJsonBody() (string,error) {
	data, err := json.Marshal(s)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (s *Sample) GetMapping() string {
	si := db.GetSearchIndex()
	si.AddProperty("name", "keyword")
	si.AddProperty("content", "text")
	si.AddProperty("age", "integer")
	si.AddProperty("time", "date")
	data, err := json.Marshal(si)
	if err != nil {
		rsrc.GetDI().GetLoger().Err(err.Error())
		return ""
	}
	return string(data)
}
