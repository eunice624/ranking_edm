package middle

import (
	"encoding/json"
	"fmt"
	"net/http"
	"ranking-edm/rsrc"
	"time"

	"github.com/betacraft/yaag/middleware"
	"github.com/globalsign/mgo/txn"
	"github.com/gorilla/mux"
)

type DebugMiddle bool

func (lm DebugMiddle) Enable() bool {
	return bool(lm)
}

func (lm DebugMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			logger := rsrc.GetDI().GetLoger()
			logger.Debug("-------Debug Request-------")
			path, _ := mux.CurrentRoute(r).GetPathTemplate()
			path = fmt.Sprintf("%s,%s?%s", r.Method, r.URL.Path, r.URL.RawQuery)
			logger.Debug("path: " + path)
			header, _ := json.Marshal(r.Header)
			logger.Debug("header: " + string(header))
			b := middleware.ReadBody(r)
			out, _ := json.Marshal(b)
			logger.Debug("body: " + string(out))

			start := time.Now()
			txn.SetDebug(lm.Enable())
			txn.SetLogger(logger.GetLogging())

			f(w, r)
			delta := time.Now().Sub(start)
			if delta.Seconds() < 3 {
				return
			}
			logger.Debug("-------End Debug Request-------")
		}
	}
}
