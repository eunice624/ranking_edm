package middle

import (
	"net/http"
)

type AuthMiddle bool

func (am AuthMiddle) Enable() bool {
	return bool(am)
}

func (am AuthMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			r.Header.Set("AuthID", "1234")
			r.Header.Set("AuthAccount", "test")
			r.Header.Set("AuthName", "test")
			f(w, r)
		}
	}
}
