package middle

import (
	"net/http"
	"runtime"

	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/google/uuid"
)

type DBMiddle bool

func (am DBMiddle) Enable() bool {
	return bool(am)
}

func (am DBMiddle) GetMiddleWare() func(f http.HandlerFunc) http.HandlerFunc {
	return func(f http.HandlerFunc) http.HandlerFunc {
		// one time scope setup area for middleware
		return func(w http.ResponseWriter, r *http.Request) {
			reqID := uuid.New()
			key := reqID.String()
			r = util.SetConnKey(r, key)
			f(w, r)
			di := rsrc.GetDI()
			di.Close(key)
			runtime.GC()
		}
	}
}
