package v1

import (
	"fmt"
	"encoding/json"
	"net/http"
	"math/rand"
	"time"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type EventAPI bool

func (a EventAPI) Enable() bool {
	return bool(a)
}

func (a EventAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/event", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/event", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/event/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/event/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/event/{ID}", Next: a.deleteEndpoint, Method: "DELETE", Auth: true},
	}
}

func (api *EventAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.EventList{}
	cm := model.GetCacheModel(redis, "3s")
	err := cm.MgoFind(&sd, bson.M{}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":    	 s.ID,
			"key": 		 s.Key,
			"Name": 	 s.Name,
			"startDate": s.StartDate.Local(),
			"endDate": 	 s.EndDate.Local(),
			"content": 	 s.Content,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *EventAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	event := &doc.Event{}
	err := json.NewDecoder(req.Body).Decode(event)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildEvent(event)
	if len(event.Key) == 0{
		errMsg += "Key not exist. "
	}
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	//檢查活動代碼是否存在
	cdb := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	csd := doc.EventCodeList{}
	cm := model.GetCacheModel(redis, "3s")
	err = cm.MgoFind(&csd, bson.M{"code": event.Key}, cdb)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Event Code " + err.Error()))
		return
	}

	if len(csd) == 0 {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Event Code not found"))
		return
	}

	//活動代碼
	code := event.Key
	event.Key = code + fmt.Sprintf("%06v", rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(1000000))

	event.EventCodeId = csd[0].ID

	currentTime := time.Now()
	event.CreatedAt = currentTime
	event.UpdatedAt = currentTime

	event.CreatedBy = req.Header.Get("AuthID")
	event.UpdatedBy = req.Header.Get("AuthID")

	db := rsrc.GetDI().GetMgodbByReq(req)
	err = event.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *EventAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	event := &doc.Event{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := event.FindByID(db, queryID)
	originEvent := *event
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(event)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildEvent(event)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	//驗證key
	if event.Key != originEvent.Key {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("key cannot be modified"))
		return
	}

	currentTime := time.Now()
	event.UpdatedAt = currentTime

	event.UpdatedBy = req.Header.Get("AuthID")

	err = event.Update(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *EventAPI) deleteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	s := &doc.Event{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = mgoDB.RemoveByID(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	//搜尋在此Event底下的Mail Template
	cacheModel := model.GetCatcheModelByReq(req, "3s")
	mgoModel := model.GetMgoDBModelByReq(req)

	m := &doc.MailTemplate{}
	mailTemp, err := cacheModel.MgoFindV2(m, bson.M{"eventid": *qid}, mgoModel)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	mailTempResult, ok := mailTemp.([]*doc.MailTemplate)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Mail Template EventId type error"))
		return
	}

	//若有資料，將這些Mail Template刪除
	if len(mailTempResult) != 0 {
		for _, m := range mailTempResult {
			if m.Status != originStatus {
				w.WriteHeader(http.StatusBadRequest)
				w.Write([]byte("Mail Template's status is SUBMIT or SEND"))
				return
			}
		}
		selector := bson.M{"eventid": *qid}
		info, err := mgoDB.RemoveAll(m, selector)
		fmt.Print(info)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Mail Template " + err.Error()))
			return
		}
	}

	w.Write([]byte("ok"))
}

func (api *EventAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	e := &doc.Event{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := e.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(e)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func vaildEvent(event *doc.Event) string {
	errMsg := ""
	if len(event.Name) == 0{
		errMsg += "Name not exist. "
	}
	if len(event.Content) == 0{
		errMsg += "Content not exist. "
	}

	return errMsg
}