package v1

import (
	"encoding/json"
	"net/http"
	"time"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

var (
	reviewStatus = map[string]interface{}{
		"UNREVIEWED": "UNREVIEWED",
		"PASS": 	"PASS",
		"REJECT": 	"REJECT",
	}
)

type ReviewAPI bool

func (a ReviewAPI) Enable() bool {
	return bool(a)
}

func (a ReviewAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/review", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/review/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/review/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/review/review-mail/{ID}", Next: a.getReviewMail, Method: "GET", Auth: true},
	}
}

func (api *ReviewAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	review := &doc.Review{}
	pipeline := []bson.M{
		bson.M{
			"$lookup": bson.M{
				"from": "mailTemplate",
				"localField": "mailid",
				"foreignField": "_id",
				"as": "mailTemplate",
			},
		},
	}
	mgoDB := model.GetMgoDBModelByReq(req)
	r, err := mgoDB.Pipeline(review, pipeline)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, ok := r.([]*doc.Review)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("type error"))
		return
	}

	if len(result) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}

	cacheModel := model.GetCatcheModelByReq(req, "3s")
	mgoModel := model.GetMgoDBModelByReq(req)

	for _, s := range result {
		//查詢Event資料
		e, err := cacheModel.MgoFindV2(&doc.Event{}, bson.M{"_id": bson.ObjectId(s.EventID)}, mgoModel)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}

		event, ok := e.([]*doc.Event)

		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("type error"))
			return
		}

		eventKey := ""
		eventName := ""
		eventContent := ""
		if len(event) > 0 {
			eventKey = event[0].Key
			eventName = event[0].Name
			eventContent = event[0].Content
		}

		var mailTemplates []map[string]interface{}
		for _, m := range s.MailTemplate {
			//查詢符合CustomerTag條件的Customer
			c, err := cacheModel.MgoFindV2(&doc.Customer{}, bson.M{"tag": bson.M{"$in": m.CustomerTag}}, mgoModel)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}

			customer, ok := c.([]*doc.Customer)

			if !ok {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("type error"))
				return
			}

			mailTemplates = append(mailTemplates, map[string]interface{}{
				"titleA": 	   m.TitleA,
				"titleB": 	   m.TitleA,
				"mailAmount":  len(customer),
				"source": 	   m.SourceID,
				"customerTag": m.CustomerTag,
				"sendTime":    m.SendTime,
				"senderName":  m.SenderName,
				"replyTo": 	   m.ReplyTo,
				"isABTest":    m.IsABTest,
			})
		}

		out = append(out, map[string]interface{}{
			"id":    	 	 s.ID,
			"key": 		 	 s.Key,
			"eventKey":  	 eventKey,
			"eventName": 	 eventName,
			"sendTime":  	 s.EventID,
			"status": 	 	 s.Status,
			"content": 	 	 eventContent,
			"mailTemplates": mailTemplates,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReviewAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	r := &doc.Review{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	if len(r.Status) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("status not exist. "))
		return
	}

	_, ok := reviewStatus[r.Status]
	if !ok || r.Status == reviewStatus["UNREVIEWED"] {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(r.Status + " is not status's option."))
		return
	}

	//整理update資料
	query := bson.M{}
	query["status"] = r.Status

	currentTime := time.Now()
	query["updatedat"] = currentTime
	query["updatedby"] = req.Header.Get("AuthID")

	err = mgoDB.UpdateByID(r, query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Write([]byte("ok"))
}

func (api *ReviewAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	review := &doc.Review{}
	pipeline := []bson.M{
		bson.M{
			"$match": bson.M{"_id": qid},
		},
		bson.M{
			"$lookup": bson.M{
				"from": "mailTemplate",
				"localField": "mailid",
				"foreignField": "_id",
				"as": "MailTemplate",
			},
		},
	}

	cacheModel := model.GetCatcheModelByReq(req, "3s")
	mgoDB := model.GetMgoDBModelByReq(req)
	r, err := mgoDB.Pipeline(review, pipeline)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, ok := r.([]*doc.Review)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("type error"))
		return
	}

	if len(result) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range result {
		//查詢Event資料
		e, err := cacheModel.MgoFindV2(&doc.Event{}, bson.M{"_id": bson.ObjectId(s.EventID)}, mgoDB)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}

		event, ok := e.([]*doc.Event)

		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("type error"))
			return
		}

		eventKey := ""
		eventName := ""
		eventContent := ""
		if len(event) > 0 {
			eventKey = event[0].Key
			eventName = event[0].Name
			eventContent = event[0].Content
		}

		var mailTemplates []map[string]interface{}
		for _, m := range s.MailTemplate {
			//查詢符合CustomerTag條件的Customer
			c, err := cacheModel.MgoFindV2(&doc.Customer{}, bson.M{"tag": bson.M{"$in": m.CustomerTag}}, mgoDB)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte(err.Error()))
				return
			}

			customer, ok := c.([]*doc.Customer)

			if !ok {
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte("type error"))
				return
			}

			mailTemplates = append(mailTemplates, map[string]interface{}{
				"titleA": 	   m.TitleA,
				"titleB": 	   m.TitleA,
				"mailAmount":  len(customer),
				"source": 	   m.SourceID,
				"customerTag": m.CustomerTag,
				"sendTime":    m.SendTime,
				"senderName":  m.SenderName,
				"replyTo": 	   m.ReplyTo,
				"isABTest":    m.IsABTest,
			})
		}

		out = append(out, map[string]interface{}{
			"id":    	 	 s.ID,
			"key": 		 	 s.Key,
			"eventKey":  	 eventKey,
			"eventName": 	 eventName,
			"sendTime":  	 s.EventID,
			"status": 	 	 s.Status,
			"content": 	 	 eventContent,
			"mailTemplates": mailTemplates,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReviewAPI) getReviewMail(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	review := &doc.Review{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(review)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	var out []map[string]interface{}
	var row []map[string]interface{}
	for _, r := range review.ReviewConfig {
		row = append(row, map[string]interface{}{
			"name": r.Name,
			"status": r.Status,
			"comment": r.Comment,
			"reviewTime": r.ReviewTime,
		})
	}
	out = append(out, map[string]interface{}{
		"id":   review.ID,
		"key": 	review.Key,
		"rows": row,
	})

	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
