package v1

import (
	"encoding/json"
	"net/http"
	"time"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type ReviewConfigAPI bool

func (a ReviewConfigAPI) Enable() bool {
	return bool(a)
}

func (a ReviewConfigAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/review-config", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/review-config", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/review-config/{ID}", Next: a.deleteEndpoint, Method: "DELETE", Auth: true},
	}
}

func (api *ReviewConfigAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.ReviewConfigList{}
	cm := model.GetCacheModel(redis, "3s")
	err := cm.MgoFind(&sd, bson.M{}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":    s.ID,
			"title": s.Title,
			"content": s.Content,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReviewConfigAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	reviewConfig := &doc.ReviewConfig{}
	err := json.NewDecoder(req.Body).Decode(reviewConfig)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := ""
	if len(reviewConfig.Title) == 0{
		errMsg += "title not exist. "
	}
	if len(reviewConfig.Content) == 0{
		errMsg += "content not exist. "
	}else{
		for _, c := range reviewConfig.Content {
			if len(c.Name) == 0 {
				errMsg += "The name in the content doesn't exist. "
			}
			if len(c.Email) == 0 {
				errMsg += "The email in the content doesn't exist. "
			}
		}
	}
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	currentTime := time.Now()
	reviewConfig.CreatedAt = currentTime
	reviewConfig.UpdatedAt = currentTime

	db := rsrc.GetDI().GetMgodbByReq(req)
	err = reviewConfig.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReviewConfigAPI) deleteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	r := &doc.ReviewConfig{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = mgoDB.RemoveByID(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	//搜尋有使用此id的event code
	cacheModel := model.GetCatcheModelByReq(req, "3s")
	mgoModel := model.GetMgoDBModelByReq(req)

	e := &doc.EventCode{}
	eventCode, err := cacheModel.MgoFindV2(e, bson.M{"reviewconfid": *qid}, mgoModel)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	codeResult, ok := eventCode.([]*doc.EventCode)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("reviewConfId type error"))
		return
	}

	//若有資料，將這些event code內的reviewconfid清空
	if len(codeResult) != 0 {
		selector := bson.M{"reviewconfid": *qid}
		_, err := mgoDB.UpdateAll(e, selector, bson.M{"reviewconfid": ""})
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Event Code " + err.Error()))
			return
		}
	}

	w.Write([]byte("ok"))
}
