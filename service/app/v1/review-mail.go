package v1

import (
	"encoding/json"
	"net/http"
	"time"
	"fmt"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

var (
	reviewMailStatus = map[string]interface{}{
		"PASS": 	"PASS",
		"REJECT": 	"REJECT",
	}
)

type ReviewMailAPI bool

func (a ReviewMailAPI) Enable() bool {
	return bool(a)
}

func (a ReviewMailAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/review-mail/{ID}", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/review-mail/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
	}
}

func (api *ReviewMailAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	review := &doc.Review{}
	pipeline := []bson.M{
		bson.M{
			"$match": bson.M{"_id": qid},
		},
		bson.M{
			"$lookup": bson.M{
				"from": "mailTemplate",
				"localField": "mailid",
				"foreignField": "_id",
				"as": "MailTemplate",
			},
		},
	}
	mgoDB := model.GetMgoDBModelByReq(req)
	r, err := mgoDB.Pipeline(review, pipeline)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, ok := r.([]*doc.Review)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("type error"))
		return
	}

	if len(result) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range result {
		fmt.Print(s)
		var row []map[string]interface{}
		for _, r := range s.ReviewConfig {
			row = append(row, map[string]interface{}{
				"name": r.Name,
				"status": r.Status,
				"comment": r.Comment,
				"reviewTime": r.ReviewTime,
			})
		}

		out = append(out, map[string]interface{}{
			"id":    	 s.ID,
			"key": 		 s.Key,
			"eventKey":  s.EventID,
			"eventName": s.EventID,
			"sendTime":  s.EventID,
			"status": 	 s.Status,
			"row": 		 row,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReviewMailAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	r := &doc.Review{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	if len(r.Status) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("status not exist. "))
		return
	}

	_, ok := reviewStatus[r.Status]
	if !ok || r.Status == reviewStatus["UNREVIEWED"] {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(r.Status + " is not status's option."))
		return
	}

	//整理update資料
	query := bson.M{}
	query["status"] = r.Status

	currentTime := time.Now()
	query["updatedat"] = currentTime
	query["updatedby"] = req.Header.Get("AuthID")

	err = mgoDB.UpdateByID(r, query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	w.Write([]byte("ok"))
}