package v1

import (
	"encoding/json"
	"net/http"
	"time"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

var (
	domainOption = map[string]interface{}{
		"Gmail":  "Gmail",
		"Amazon": "Amazon",
	}
)

type SendAccountAPI bool

func (a SendAccountAPI) Enable() bool {
	return bool(a)
}

func (a SendAccountAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/send-account", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/send-account", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/send-account/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/send-account/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/send-account/{ID}", Next: a.deleteEndpoint, Method: "DELETE", Auth: true},
	}
}

func (api *SendAccountAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.SendAccountList{}
	cm := model.GetCacheModel(redis, "3s")
	err := cm.MgoFind(&sd, bson.M{}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":	   s.ID,
			"name":    s.Name,
			"account": s.Account,
			"domain":  s.Domain,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *SendAccountAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {

	sendAccount := &doc.SendAccount{}
	err := json.NewDecoder(req.Body).Decode(sendAccount)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildSendAccount(sendAccount)
	if len(sendAccount.Password) == 0{
		errMsg += "Password not exist. "
	}else {
		if len(sendAccount.Password) < 4 {
			errMsg += "Minimum 4 characters in length"
		}
	}

	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	currentTime := time.Now()
	sendAccount.CreatedAt = currentTime
	sendAccount.UpdatedAt = currentTime

	sendAccount.CreatedBy = req.Header.Get("AuthID")
	sendAccount.UpdatedBy = req.Header.Get("AuthID")

	db := rsrc.GetDI().GetMgodbByReq(req)
	err = sendAccount.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *SendAccountAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	sendAccount := &doc.SendAccount{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := sendAccount.FindByID(db, queryID)
	oriSendAccount := *sendAccount
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(sendAccount)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildSendAccount(sendAccount)
	if len(sendAccount.Password) == 0{
		sendAccount.Password = oriSendAccount.Password
	}else {
		if len(sendAccount.Password) < 4 {
			errMsg += "Minimum 4 characters in length"
		}
	}

	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	currentTime := time.Now()
	sendAccount.UpdatedAt = currentTime

	sendAccount.UpdatedBy = req.Header.Get("AuthID")

	err = sendAccount.Update(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *SendAccountAPI) deleteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	s := &doc.SendAccount{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = mgoDB.RemoveByID(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *SendAccountAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	s := &doc.SendAccount{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := s.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func vaildSendAccount(sendAccount *doc.SendAccount) string {
	errMsg := ""
	if len(sendAccount.Name) == 0{
		errMsg += "Name not exist. "
	}
	if len(sendAccount.Account) == 0{
		errMsg += "Account not exist. "
	}
	if len(sendAccount.Domain) == 0{
		errMsg += "Domain not exist. "
	}else {
		if _, ok := domainOption[sendAccount.Domain]; !ok {
			errMsg += sendAccount.Domain + " is not in the Domain options. "
		}
	}

	return errMsg
}
