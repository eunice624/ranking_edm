package v1

import (
	"encoding/json"
	"net/http"
	"time"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type ReplyMailAPI bool

func (a ReplyMailAPI) Enable() bool {
	return bool(a)
}

func (a ReplyMailAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/reply-mail", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/reply-mail", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/reply-mail/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/reply-mail/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/reply-mail/{ID}", Next: a.deleteEndpoint, Method: "DELETE", Auth: true},
	}
}

func (api *ReplyMailAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.ReplyMailList{}
	cm := model.GetCacheModel(redis, "3s")
	err := cm.MgoFind(&sd, bson.M{}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":    s.ID,
			"email": s.Email,
			"comment": s.Comment,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *ReplyMailAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	replyMail := &doc.ReplyMail{}
	err := json.NewDecoder(req.Body).Decode(replyMail)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildReplyMail(replyMail)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	currentTime := time.Now()
	replyMail.CreatedAt = currentTime
	replyMail.UpdatedAt = currentTime

	replyMail.CreatedBy = req.Header.Get("AuthID")
	replyMail.UpdatedBy = req.Header.Get("AuthID")

	db := rsrc.GetDI().GetMgodbByReq(req)
	err = replyMail.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReplyMailAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	replyMail := &doc.ReplyMail{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := replyMail.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(replyMail)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildReplyMail(replyMail)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	currentTime := time.Now()
	replyMail.UpdatedAt = currentTime

	replyMail.UpdatedBy = req.Header.Get("AuthID")

	err = replyMail.Update(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReplyMailAPI) deleteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	r := &doc.ReplyMail{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = mgoDB.RemoveByID(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *ReplyMailAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	r := &doc.ReplyMail{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := r.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(r)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func vaildReplyMail(replyMail *doc.ReplyMail) string {
	errMsg := ""
	if len(replyMail.Email) == 0{
		errMsg += "Email not exist. "
	}
	if len(replyMail.Comment) == 0{
		errMsg += "Comment not exist. "
	}

	return errMsg
}