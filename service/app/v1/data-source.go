package v1

import (
	"encoding/json"
	"net/http"
	"time"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type SourceAPI bool

func (a SourceAPI) Enable() bool {
	return bool(a)
}

func (a SourceAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		//&api.APIHandler{Path: "/v1/data-source", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/data-source", Next: a.createEndpoint, Method: "POST", Auth: true},
	}
}

func (api *SourceAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.SourceList{}
	cm := model.GetCacheModel(redis, "5m")
	err := cm.MgoFind(&sd, bson.M{}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":    	   s.ID,
			"source": 	   s.Source,
			"tagURL": 	   s.TagURL,
			"customerURL": s.CustomerURL,
			"senderURL":   s.SenderURL,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *SourceAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	source := &doc.Source{}
	err := json.NewDecoder(req.Body).Decode(source)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildSource(source)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	currentTime := time.Now()
	source.CreatedAt = currentTime
	source.UpdatedAt = currentTime

	source.CreatedBy = req.Header.Get("AuthID")
	source.UpdatedBy = req.Header.Get("AuthID")

	db := rsrc.GetDI().GetMgodbByReq(req)
	err = source.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *SourceAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	source := &doc.Source{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := source.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(source)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildSource(source)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	currentTime := time.Now()
	source.UpdatedAt = currentTime

	source.UpdatedBy = req.Header.Get("AuthID")

	err = source.Update(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *SourceAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	s := &doc.Source{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := s.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func vaildSource(source *doc.Source) string {
	errMsg := ""
	if len(source.Source) == 0{
		errMsg += "Name not exist. "
	}
	if len(source.CustomerURL) == 0{
		errMsg += "CustomerURL not exist. "
	}
	if len(source.TagURL) == 0{
		errMsg += "TagURL not exist. "
	}
	if len(source.SenderURL) == 0{
		errMsg += "SenderURL not exist. "
	}

	return errMsg
}