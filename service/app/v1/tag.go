package v1

import (
	"encoding/json"
	"net/http"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type TagAPI bool

func (a TagAPI) Enable() bool {
	return bool(a)
}

func (a TagAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/tag", Next: a.getEndpoint, Method: "GET", Auth: true},
		//&api.APIHandler{Path: "/v1/tag", Next: a.createEndpoint, Method: "POST", Auth: true},
	}
}

func (api *TagAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.TagList{}
	cm := model.GetCacheModel(redis, "5s")
	err := cm.MgoFind(&sd, bson.M{}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":    	s.ID,
			"name": 	s.Name,
			"sourceID": s.SourceID,
			"subTag": 	s.SubTag,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *TagAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {

	tag := &doc.Tag{}
	err := json.NewDecoder(req.Body).Decode(tag)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err = tag.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *TagAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	t := &doc.Tag{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := t.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(t)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
