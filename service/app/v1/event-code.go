package v1

import (
	"encoding/json"
	"net/http"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type EventCodeAPI bool

func (a EventCodeAPI) Enable() bool {
	return bool(a)
}

func (a EventCodeAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/event-code", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/event-code", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/event-code/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/event-code/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/event-code/{ID}", Next: a.deleteEndpoint, Method: "DELETE", Auth: true},
	}
}

func (api *EventCodeAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	eventCode := &doc.EventCode{}
	pipeline := []bson.M{
		bson.M{
			"$lookup": bson.M{
				"from": "reviewConfig",
				"localField": "reviewconfid",
				"foreignField": "_id",
				"as": "reviewConfig",
			},
		},
	}
	mgoDB := model.GetMgoDBModelByReq(req)
	e, err := mgoDB.Pipeline(eventCode, pipeline)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, ok := e.([]*doc.EventCode)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("type error"))
		return
	}
	if len(result) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range result {
		out = append(out, map[string]interface{}{
			"id":    		s.ID,
			"code": 		s.Code,
			"comment": 		s.Comment,
			"reviewConfig": map[string]interface{}{
				"id": 	 s.ReviewConfig[0].ID,
				"title": s.ReviewConfig[0].Title,
			},
		})
	}

	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *EventCodeAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	eventCode := &doc.EventCode{}
	err := json.NewDecoder(req.Body).Decode(eventCode)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildEventCode(eventCode)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	//檢查簽核流程是否存在
	r := &doc.ReviewConfig{}
	rdb := rsrc.GetDI().GetMgodbByReq(req)
	err = r.FindByID(rdb, eventCode.ReviewConfID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Review Config " + err.Error()))
		return
	}

	db := rsrc.GetDI().GetMgodbByReq(req)
	err = eventCode.Save(req, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *EventCodeAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	eventCode := &doc.EventCode{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := eventCode.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(eventCode)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildEventCode(eventCode)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	//檢查簽核流程是否存在
	r := &doc.ReviewConfig{}
	rdb := rsrc.GetDI().GetMgodbByReq(req)
	err = r.FindByID(rdb, eventCode.ReviewConfID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Review Config " + err.Error()))
		return
	}

	err = eventCode.Update(req, db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *EventCodeAPI) deleteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	s := &doc.EventCode{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = mgoDB.RemoveByID(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *EventCodeAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	e := &doc.EventCode{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := e.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(e)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func vaildEventCode(eventCode *doc.EventCode) string {
	errMsg := ""
	if len(eventCode.Code) == 0{
		errMsg += "Code not exist. "
	}
	if len(eventCode.Comment) == 0{
		errMsg += "Comment not exist. "
	}
	if len(eventCode.ReviewConfig) != 0{
		errMsg += "ReviewConfig cannot exist. "
	}

	return errMsg
}
