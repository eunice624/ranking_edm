package v1

import (
	"encoding/json"
	"net/http"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type SampleAPI bool

func (a SampleAPI) Enable() bool {
	return bool(a)
}

func (a SampleAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/sample", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/sample", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/sample/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
	}
}

func (api *SampleAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.SampleList{}
	cm := model.GetCacheModel(redis, "3s")
	err := cm.MgoFind(&sd, bson.M{}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":    s.ID,
			"title": s.Title,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *SampleAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	sample := &doc.Sample{}
	err := json.NewDecoder(req.Body).Decode(sample)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err = sample.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *SampleAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	s := &doc.Sample{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := s.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
