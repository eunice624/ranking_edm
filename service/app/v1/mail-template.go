package v1

import (
	"encoding/json"
	"net/http"
	"math/rand"
	"time"
	"fmt"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/rsrc"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

var (
	originStatus = "ORIGIN"
	submitStatus = "SUBMIT"
	passStatus = "PASS"
	rejectStatus = "REJECT"
	sendStatus = "SEND"
)

type MailTempAPI bool

func (a MailTempAPI) Enable() bool {
	return bool(a)
}

func (a MailTempAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/mail-template", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/mail-template", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/mail-template/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/mail-template/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/mail-template/{ID}", Next: a.deleteEndpoint, Method: "DELETE", Auth: true},
		&api.APIHandler{Path: "/v1/mail-template/read/{ID}", Next: a.updateReadEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/mail-template/_submit", Next: a.createSubmitEndpoint, Method: "POST", Auth: true},
	}
}

func (api *MailTempAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	urlQuery := req.URL.Query()
	queryEventID := urlQuery.Get("eventID")
	if(queryEventID == ""){
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("eventID not exist."))
		return
	}

	qid, err := doc.GetObjectID(queryEventID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	db := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	sd := doc.MailTempList{}
	cm := model.GetCacheModel(redis, "3s")
	err = cm.MgoFind(&sd, bson.M{"eventid": *qid}, db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if len(sd) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range sd {
		out = append(out, map[string]interface{}{
			"id":    	   s.ID,
			"titleA": 	   s.TitleA,
			"titleB": 	   s.TitleB,
			"source": 	   s.SourceID,
			"customerTag": s.CustomerTag,
			"sendTime":    s.SendTime,
			"senderName":  s.SenderName,
			"replyTo": 	   s.ReplyTo,
			"isABTest":    s.IsABTest,
			"isRead":      s.IsRead,
			"status": 	   s.Status,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *MailTempAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	mailTemplate := &doc.MailTemplate{}
	err := json.NewDecoder(req.Body).Decode(mailTemplate)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildMailTemp(mailTemplate)
	if len(mailTemplate.EventID) == 0{
		errMsg += "Event not exist. "
	}
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	//檢查分類是否存在
	for _, c := range mailTemplate.CustomerTag {
		cacheModel := model.GetCatcheModelByReq(req, "3s")
		mgoModel := model.GetMgoDBModelByReq(req)

		q := bson.M{"subtag": bson.M{"$all": []string{c}}, "sourceid": mailTemplate.SourceID}
		r, err := cacheModel.MgoFindV2(&doc.Tag{}, q, mgoModel)

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}

		result, ok := r.([]*doc.Tag)

		if !ok {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("type error"))
			return
		}

		if len(result) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Customer Tag not found"))
			return
		}
	}

	//檢查資料來源是否存在
	s := &doc.Source{}
	sdb := rsrc.GetDI().GetMgodbByReq(req)
	err = s.FindByID(sdb, mailTemplate.SourceID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Source " + err.Error()))
		return
	}

	//檢查event ID
	eid, err := doc.GetObjectID(mailTemplate.EventID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Event " + err.Error()))
		return
	}
	//檢查event是否存在
	e := &doc.Event{ID: *eid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(e)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Event " + err.Error()))
		return
	}

	//狀態
	mailTemplate.Status = originStatus
	//已讀
	mailTemplate.IsRead = false

	mailTemplate.EventCodeId = e.EventCodeId

	currentTime := time.Now()
	mailTemplate.CreatedAt = currentTime
	mailTemplate.UpdatedAt = currentTime

	mailTemplate.CreatedBy = req.Header.Get("AuthID")
	mailTemplate.UpdatedBy = req.Header.Get("AuthID")

	db := rsrc.GetDI().GetMgodbByReq(req)
	err = mailTemplate.Save(db)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *MailTempAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	mailTemplate := &doc.MailTemplate{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := mailTemplate.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if mailTemplate.Status == sendStatus {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("This Mail Template's status is SEND"))
		return
	}

	err = json.NewDecoder(req.Body).Decode(mailTemplate)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildMailTemp(mailTemplate)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	//檢查分類是否存在
	for _, c := range mailTemplate.CustomerTag {
		cdb := rsrc.GetDI().GetMgodbByReq(req)
		redis := rsrc.GetDI().GetRedisByReq(req)
		csd := doc.TagList{}
		cm := model.GetCacheModel(redis, "3s")
		err = cm.MgoFind(&csd, bson.M{"subtag": bson.M{"$all": []string{c}}, "sourceid": mailTemplate.SourceID}, cdb)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Customer Tag " + err.Error()))
			return
		}

		if len(csd) == 0 {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Customer Tag not found"))
			return
		}
	}

	//檢查資料來源是否存在
	s := &doc.Source{}
	sdb := rsrc.GetDI().GetMgodbByReq(req)
	err = s.FindByID(sdb, mailTemplate.SourceID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Source " + err.Error()))
		return
	}

	//狀態
	mailTemplate.Status = originStatus
	//已讀
	mailTemplate.IsRead = false

	currentTime := time.Now()
	mailTemplate.UpdatedAt = currentTime

	mailTemplate.UpdatedBy = req.Header.Get("AuthID")

	err = mailTemplate.Update(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *MailTempAPI) deleteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	m := &doc.MailTemplate{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(m)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	if(m.Status != originStatus){
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Status is " + m.Status))
		return
	}

	err = mgoDB.RemoveByID(m)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *MailTempAPI) updateReadEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	mailTemplate := &doc.MailTemplate{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := mailTemplate.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	//已讀
	mailTemplate.IsRead = true

	currentTime := time.Now()
	mailTemplate.UpdatedAt = currentTime

	mailTemplate.UpdatedBy = req.Header.Get("AuthID")

	err = mailTemplate.Update(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *MailTempAPI) createSubmitEndpoint(w http.ResponseWriter, req *http.Request) {
	submitArray := &doc.SubmitArray{}
	err := json.NewDecoder(req.Body).Decode(submitArray)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//檢查Mail Template ID是否正確
	var objectIDs []interface{}
	for _, id := range *submitArray {
		if !bson.IsObjectIdHex(id) {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("id is error: " + id))
			return
		}
		objectIDs = append(objectIDs, id)
	}

	//檢查Mail Template ID是否存在
	mdb := rsrc.GetDI().GetMgodbByReq(req)
	redis := rsrc.GetDI().GetRedisByReq(req)
	msd := doc.MailTempList{}
	cm := model.GetCacheModel(redis, "3s")

	for _, objectID := range objectIDs {
		id := bson.ObjectIdHex(objectID.(string))
		err = cm.MgoFind(&msd, bson.M{"_id": id}, mdb)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Mail Template" + err.Error()))
			return
		}
		if len(msd) == 0 {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("id is not exist: " + objectID.(string)))
			return
		}

		//檢查Mail Template是否已讀
		if msd[0].IsRead == false {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Mail Template's content has not been read: " + objectID.(string)))
			return
		}
		//檢查Mail Template狀態
		if msd[0].Status == sendStatus {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("Mail Template's status is SEND: " + objectID.(string)))
			return
		}
	}

	//更新Mail Template狀態為SEND
	currentTime := time.Now()
	query := bson.M{
		"status": submitStatus,
		"updatedat": currentTime,
		"updatedby": req.Header.Get("AuthID"),
	}
	db := rsrc.GetDI().GetMgodbByReq(req)
	_, err = doc.UpdateAll(db, objectIDs, query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	//從Event Code找Review Config
	eventCode := &doc.EventCode{}
	pipeline := []bson.M{
		bson.M{
			"$match": bson.M{"_id": bson.ObjectId(msd[0].EventCodeId)},
		},
		bson.M{
			"$lookup": bson.M{
				"from": "reviewConfig",
				"localField": "reviewconfid",
				"foreignField": "_id",
				"as": "reviewConfig",
			},
		},
	}
	mgoDB := model.GetMgoDBModelByReq(req)
	e, err := mgoDB.Pipeline(eventCode, pipeline)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, ok := e.([]*doc.EventCode)
	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("type error"))
		return
	}

	if len(result) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("event code not exist"))
		return
	}

	//change Mail Template ID Array type
	var mailIds []bson.ObjectId
	for _, id := range objectIDs{
		mailIds = append(mailIds, bson.ObjectIdHex(id.(string)))
	}

	//新增簽核單
	status := fmt.Sprint(reviewStatus["UNREVIEWED"])

	review := &doc.Review{}
	review.Key = "R" + result[0].Code + fmt.Sprintf("%08v", rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(100000000))
	review.EventID = msd[0].EventID
	review.MailID = mailIds
	review.CurrentReview = 0
	review.Status = status

	if len(result[0].ReviewConfig) > 0 {
		for _, r := range result[0].ReviewConfig[0].Content {
			reviewConfContent := doc.ReviewConfContent{
				Name: r.Name,
				Email: r.Email,
				Status: status,
			}
			review.ReviewConfig = append(review.ReviewConfig, reviewConfContent)
		}
	}

	review.CreatedAt = currentTime
	review.UpdatedAt = currentTime
	review.CreatedBy = req.Header.Get("AuthID")
	review.UpdatedBy = req.Header.Get("AuthID")

	err = mgoDB.Save(review)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Review " + err.Error()))
		return
	}

	w.Write([]byte("ok"))
}

func (api *MailTempAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	m := &doc.MailTemplate{}
	db := rsrc.GetDI().GetMgodbByReq(req)
	err := m.FindByID(db, queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(m)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func vaildMailTemp(mailTemplate *doc.MailTemplate) string {
	errMsg := ""
	if len(mailTemplate.TitleA) == 0{
		errMsg += "TitleA not exist. "
	}
	//Title B
	if mailTemplate.IsABTest {
		if len(mailTemplate.TitleB) == 0{
			errMsg += "TitleB not exist. "
		}
	}else{
		mailTemplate.TitleB = ""
	}
	if len(mailTemplate.SourceID) == 0{
		errMsg += "Source not exist. "
	}
	if len(mailTemplate.CustomerTag) == 0{
		errMsg += "CustomerTag not exist. "
	}
	if len(mailTemplate.SenderName) == 0{
		errMsg += "SenderName not exist. "
	}
	if mailTemplate.SendTime.Before(time.Now()) {
		errMsg += "Send Time cannot be before current time. "
	}

	return errMsg
}
