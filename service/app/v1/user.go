package v1

import (
	"encoding/json"
	"net/http"
	"encoding/hex"
	"crypto/md5"
	"crypto/sha256"
	"time"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type UserAPI bool

func (a UserAPI) Enable() bool {
	return bool(a)
}

func (a UserAPI) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v1/user", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/user", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v1/user/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v1/user/{ID}", Next: a.updateEndpoint, Method: "PUT", Auth: true},
		&api.APIHandler{Path: "/v1/user/{ID}", Next: a.deleteEndpoint, Method: "DELETE", Auth: true},
	}
}

func (api *UserAPI) getEndpoint(w http.ResponseWriter, req *http.Request) {
	cacheModel := model.GetCatcheModelByReq(req, "3s")
	mgoModel := model.GetMgoDBModelByReq(req)

	r, err := cacheModel.MgoFindV2(&doc.User{}, bson.M{}, mgoModel)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, ok := r.([]*doc.User)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("type error"))
		return
	}

	if len(result) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range result {
		out = append(out, map[string]interface{}{
			"id":    	  s.ID,
			"name": 	  s.Name,
			"email": 	  s.Email,
			"permission": s.Permission,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *UserAPI) createEndpoint(w http.ResponseWriter, req *http.Request) {
	user := &doc.User{}
	err := json.NewDecoder(req.Body).Decode(user)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	//欄位驗證
	errMsg := vaildUser(user)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}

	//密碼加密
	user.Password = encryptUserPassword(user.Password)

	currentTime := time.Now()
	user.CreatedAt = currentTime
	user.UpdatedAt = currentTime

	user.CreatedBy = req.Header.Get("AuthID")
	user.UpdatedBy = req.Header.Get("AuthID")

	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.Save(user)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *UserAPI) updateEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	u := &doc.User{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(u)
	oriU := *u
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = json.NewDecoder(req.Body).Decode(u)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	////欄位驗證
	errMsg := vaildUser(u)
	if errMsg != ""{
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(errMsg))
		return
	}
	if(u.Password != oriU.Password){
		u.Password = encryptUserPassword(u.Password)
	}

	//整理update資料
	data, err := bson.Marshal(&u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	query := bson.M{}
	err = bson.Unmarshal(data, query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	currentTime := time.Now()
	query["updatedat"] = currentTime
	query["updatedby"] = req.Header.Get("AuthID")

	err = mgoDB.UpdateByID(u, query)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *UserAPI) deleteEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]
	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	u := &doc.User{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	err = mgoDB.RemoveByID(u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *UserAPI) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	u := &doc.User{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(u)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

//驗證欄位
func vaildUser(user *doc.User) string {
	errMsg := ""
	if len(user.Name) == 0{
		errMsg += "Name not exist. "
	}
	if len(user.Email) == 0{
		errMsg += "Email not exist. "
	}
	if len(user.Password) == 0{
		errMsg += "Password not exist. "
	}

	return errMsg
}

//密碼加密
func encryptUserPassword(password string) string  {
	//md5
	enMd5 := md5.New()
	enMd5.Write([]byte(password))
	enPassword := hex.EncodeToString(enMd5.Sum(nil))

	//salt
	salt1 := "ranking"
	salt2 := "edm"
	enPassword = salt1 + enPassword + salt2

	//sha256
	enSHA := sha256.New()
	enSHA.Write([]byte(enPassword))
	enPassword = hex.EncodeToString(enSHA.Sum(nil))

	return enPassword
}

