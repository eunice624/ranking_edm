package v1

import (
	"encoding/json"
	"net/http"

	"ranking-edm/api"
	"ranking-edm/doc"
	"ranking-edm/model"
	"ranking-edm/util"

	"github.com/globalsign/mgo/bson"
)

type Sample2API bool

func (a Sample2API) Enable() bool {
	return bool(a)
}

func (a Sample2API) GetAPIs() *[]*api.APIHandler {
	return &[]*api.APIHandler{
		&api.APIHandler{Path: "/v2/sample", Next: a.getEndpoint, Method: "GET", Auth: true},
		&api.APIHandler{Path: "/v2/sample", Next: a.createEndpoint, Method: "POST", Auth: true},
		&api.APIHandler{Path: "/v2/sample/{ID}", Next: a.detailEndpoint, Method: "GET", Auth: true},
	}
}

func (api *Sample2API) getEndpoint(w http.ResponseWriter, req *http.Request) {
	cacheModel := model.GetCatcheModelByReq(req, "3s")
	mgoModel := model.GetMgoDBModelByReq(req)

	r, err := cacheModel.MgoFindV2(&doc.Sample2{}, bson.M{}, mgoModel)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	result, ok := r.([]*doc.Sample2)

	if !ok {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("type error"))
		return
	}

	if len(result) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	var out []map[string]interface{}
	for _, s := range result {
		out = append(out, map[string]interface{}{
			"id":    s.ID,
			"title": s.Title,
		})
	}
	err = json.NewEncoder(w).Encode(out)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}

func (api *Sample2API) createEndpoint(w http.ResponseWriter, req *http.Request) {

	sample := &doc.Sample2{}
	err := json.NewDecoder(req.Body).Decode(sample)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.Save(sample)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte("ok"))
}

func (api *Sample2API) detailEndpoint(w http.ResponseWriter, req *http.Request) {
	vars := util.GetPathVars(req, []string{"ID"})
	queryID := vars["ID"]

	qid, err := doc.GetObjectID(queryID)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	s := &doc.Sample2{ID: *qid}
	mgoDB := model.GetMgoDBModelByReq(req)
	err = mgoDB.FindByID(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(w).Encode(s)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
}
