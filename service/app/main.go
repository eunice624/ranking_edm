package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"path/filepath"
	"ranking-edm/api"
	"ranking-edm/rsrc"

	"ranking-edm/service/app/middle"
	appV1 "ranking-edm/service/app/v1"

	"github.com/gorilla/mux"
)

var (
	confpath = flag.String("confpath", "./", "write memory profile to `file`")
	v        = flag.Bool("v", false, "version")

	Version   = "1.0.0"
	BuildTime = "2000-01-01T00:00:00+0800"
)

func main() {
	flag.Parse()

	if *v {
		fmt.Println("Version: " + Version)
		fmt.Println("Build Time: " + BuildTime)
		return
	}

	filename, _ := filepath.Abs(*confpath + "config.yml")
	rsrc.InitDI(filename)
	runAPI()
}

func runAPI() {

	router := mux.NewRouter()
	di := rsrc.GetDI()
	//middleConf := di.APIConf.Middle
	middlewares := api.GetMiddlewares(
		middle.DebugMiddle(true),
		middle.DBMiddle(true),
		middle.AuthMiddle(true),
	)
	myAPI := &api.MyAPI{Router: router, MiddleWares: middlewares}

	api.InitAPI(myAPI, appV1.UserAPI(true), appV1.CustomerAPI(true),
		appV1.TagAPI(true), appV1.SendAccountAPI(true),
		appV1.ReplyMailAPI(true), appV1.SourceAPI(true),
		appV1.EventCodeAPI(true),appV1.ReviewConfigAPI(true),
		appV1.ReviewAPI(true), appV1.ReviewMailAPI(true),
		appV1.EventAPI(true), appV1.MailTempAPI(true))

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", di.APIConf.Port), router))
}
