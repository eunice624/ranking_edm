package db

import (
	"strings"
	"time"

	"github.com/globalsign/mgo"
)

var (
	TxnC = "txn"
)

const (
	normalTimeout = time.Second * 60
	batchTimeout  = time.Second * 300
	MODE_READ     = iota
	MODE_WRITE
)

type Mongo struct {
	Host     string `yaml:"host"`
	Port     string `yaml:"port"`
	User     string `yaml:"user"`
	Pass     string `yaml:"pass"`
	Database string `yaml:"database"`

	_session *mgo.Session
	connPool map[string]*mgo.Session
}

func (m *Mongo) GetDB(key string) *mgo.Database {
	if m.connPool == nil {
		m.connPool = make(map[string]*mgo.Session)
	}
	c, ok := m.connPool[key]
	if ok {
		return c.DB(m.Database)
	}
	c = m.connect()
	m.connPool[key] = c
	return c.DB(m.Database)
}

func (m *Mongo) Close(key string) {
	c, ok := m.connPool[key]
	if ok {
		c.Close()
		delete(m.connPool, key)
	}
}

func (m *Mongo) connect() *mgo.Session {
	var session *mgo.Session
	if m._session != nil {
		session = m._session.Clone()
	}
	if session != nil {
		return session
	}

	var address []string
	address = strings.SplitN(m.Host, ",", -1)
	dialInfo := mgo.DialInfo{
		Addrs:         address,
		Database:      m.Database,
		Timeout:       60 * time.Second,
		MaxIdleTimeMS: 2000,
		MinPoolSize:   1000,
	}
	if m.User != "" && m.Pass != "" {
		dialInfo.Username = m.User
		dialInfo.Password = m.Pass
	}

	session, err := mgo.DialWithInfo(&dialInfo)
	if err != nil {
		panic(err)
	}
	session.SetPoolLimit(500)
	session.SetSocketTimeout(normalTimeout)
	session.SetMode(mgo.Monotonic, true)

	// 註解掉debug訊息
	// mgo.SetDebug(true)
	// var aLogger *log.Logger
	// aLogger = log.New(os.Stderr, "", log.LstdFlags)
	// mgo.SetLogger(aLogger)
	m._session = session
	return session.Clone()
}

func (m *Mongo) Ping() error {
	session := m.connect()
	defer session.Close()
	return session.Ping()
}
