package db

import (
	"fmt"
	"testing"
	"encoding/json"

	"github.com/stretchr/testify/assert"
)


func Test_Index(t *testing.T) {
	i:=GetSearchIndex()
	i.AddProperty("age", "integer")
	i.AddProperty("email", "keyword")
	data, err := json.Marshal(i)
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(string(data))
	assert.False(t, true)
}