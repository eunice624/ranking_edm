package db

import (
	"github.com/go-redis/redis"
)

const (
	modmusDB = 1
)

type Redis struct {
	Host string `yaml:"host,omitempty"`
	DB   int    `yaml:"db,omitempty"`

	connPool map[string]*redis.Client
}

func (r *Redis) GetClient(key string) *redis.Client {
	if r.connPool == nil {
		r.connPool = make(map[string]*redis.Client)
	}
	c, ok := r.connPool[key]
	if ok {
		return c
	}
	c = redis.NewClient(&redis.Options{
		Addr: r.Host,
		DB:   r.DB, // use default DB
	})
	r.connPool[key] = c
	return c
}

func (r *Redis) Close(k string) {
	c, ok := r.connPool[k]
	if ok {
		c.Close()
		delete(r.connPool, k)
	}
}
