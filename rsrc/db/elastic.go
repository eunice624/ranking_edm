package db

import (
	"errors"
	"time"
	"ranking-edm/util"
	"context"

	elastic "gopkg.in/olivere/elastic.v7"
)

type Search struct {
	Hosts []string `yaml:"host"`
	Port  string   `yaml:"port"`
	Index string   `yaml:"index"`

	connPool map[string]*elastic.Client

	dateKey string
	indexExistsMap map[string]bool
}

const (
	dateFormat = "060102"
)
func (s *Search) GetIndexByDate(_type string) string {
	dk := time.Now().Format(dateFormat)
	return util.StrAppend(s.Index,"+", _type, "-", dk)
}

func (s *Search) GetClient(key string) *elastic.Client {
	if s.connPool == nil {
		s.connPool = make(map[string]*elastic.Client)
	}
	c, ok := s.connPool[key]
	if ok {
		return c
	}
	c=s.connect()
	s.connPool[key] = c
	return c
}

// 檢查每日index是否存在
func (sm *Search) ExistsTypeByDate(key string, _type string) bool {
	dk := time.Now().Format(dateFormat)
	if dk != sm.dateKey {
		sm.dateKey = dk
		sm.indexExistsMap = make(map[string]bool)
	}

	indexWithDate := sm.GetIndexByDate(_type)
	if exists,_ := sm.indexExistsMap[indexWithDate]; exists {
		return true
	}

	
	c := sm.GetClient(key)
	if c == nil {
		panic("client is nil")
	}
	exists, err := c.IndexExists(indexWithDate).Do(context.Background())

	if err != nil {
		return false
	}

	if !exists {
		return false
	}

	sm.indexExistsMap[indexWithDate] = true
	return true

}

func (sm *Search) CreateTypeByDate(key string, _type string, mapping string) error {
	indexWithDate := sm.GetIndexByDate(_type)
	c := sm.GetClient(key)
	createIndex, err := c.CreateIndex(indexWithDate).Body(mapping).Do(context.Background())
	if err != nil {
		return err
	}

	if !createIndex.Acknowledged {
		return errors.New("cant create index.")
	}
	return nil
}


func (s *Search) connect() *elastic.Client {
	myclient, err := elastic.NewClient(elastic.SetURL(s.Hosts...), elastic.SetSniff(false))
	if err != nil {
		panic(err)
	}
	return myclient
}

func (s *Search) Close(key string) {
	delete(s.connPool, key)
}


type index struct {
	Mappings *mappings `json:"mappings"`
}

func GetSearchIndex() *index {
	return &index{
		Mappings: &mappings{
			Properties: make(map[string]*property),
		},
	}
} 

type mappings struct {
	Properties map[string]*property `json:"properties"`
}

type property struct {
	Type   string `json:"type"`
}

func (p *index) AddProperty(key string, _type string) {
	if p.Mappings == nil {
		p.Mappings = &mappings{
			Properties: make(map[string]*property),
		}
	}
	pr := &property{
		Type: _type,
	}
	p.Mappings.Properties[key] = pr
}