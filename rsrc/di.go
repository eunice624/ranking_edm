package rsrc

import (
	"io/ioutil"
	"net/http"
	"path/filepath"
	"time"

	"ranking-edm/api"
	"ranking-edm/rsrc/db"
	"ranking-edm/util"

	"github.com/globalsign/mgo"
	"github.com/go-redis/redis"
	yaml "gopkg.in/yaml.v2"
)

type di struct {
	Log     *Logger   `yaml:"log,omitempty"`
	Mongodb *db.Mongo `yaml:"mongo,omitempty"`
	Redisdb *db.Redis `yaml:"redis,omitempty"`

	APIConf *api.APIConf `yaml:"api,omitempty"`

	confPath string
	location *time.Location
}

func (d *di) GetMgodbByReq(r *http.Request) *mgo.Database {
	if d == nil || d.Mongodb == nil {
		panic("not init di")
	}
	key := util.GetConnKey(r)
	return d.Mongodb.GetDB(key)
}

func (d *di) GetRedisByReq(r *http.Request) *redis.Client {
	if d == nil || d.Redisdb == nil {
		panic("not init di")
	}
	key := util.GetConnKey(r)
	return d.Redisdb.GetClient(key)
}

func (d *di) Close(key string) {
	if d.Mongodb != nil {
		d.Mongodb.Close(key)
	}
	if d.Redisdb != nil {
		d.Redisdb.Close(key)
	}
}

func (d *di) GetLoger() *Logger {
	if d == nil || d.Log == nil {
		panic("not init di")
	}
	return d.Log
}

var myDI *di

func InitDI(fp string) {
	yamlFile, err := ioutil.ReadFile(fp)

	if err != nil {
		panic(err)
	}
	d := di{}
	err = yaml.Unmarshal(yamlFile, &d)
	if err != nil {
		panic(err)
	}

	loc, err := time.LoadLocation("Asia/Taipei")
	if err != nil {
		panic(err)
	}

	d.confPath = util.StrAppend(filepath.Dir(fp), "/")
	d.location = loc
	d.Log.StartLog()
	myDI = &d

}

func GetDI() *di {
	if myDI == nil {
		panic("not init di")
	}
	return myDI
}
