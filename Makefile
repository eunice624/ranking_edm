VERSION=`git describe --tags`
BUILD_TIME=`date +%FT%T%z`
LDFLAGS=-ldflags "-X main.Version=${V} -X main.BuildTime=${BUILD_TIME}"


run: clear
	go build ${LDFLAGS} -o ./bin/$(SER) ./service/$(SER)/main.go
	./bin/$(SER) -v
	./bin/$(SER) -confpath ./service/$(SER)/config/etc/config-${ENV}/

build: clear
	go build ${LDFLAGS} -o ./bin/$(SER) ./service/$(SER)/main.go
	./bin/$(SER) -v

build-win: clear-win
	GOOS=windows GOARCH=386 go build -o ./build/exe/$(SER).exe ./service/$(SER)/main.go

clear-win:
	rm -rf ./build/exe/*

clear:
	rm -rf ./bin/$(SER)